# Recurrence
A lambda function for generating recurring events using [dmfs/lib-recur](https://github.com/dmfs/lib-recur)
for generating [RRULEs](https://icalendar.org/iCalendar-RFC-5545/3-8-5-3-recurrence-rule.html).

## UI Demo
This lambda was built for the sole purpose of demoing my angular
recurrence builder project. A CloudFront link can be found [here](http://d2iza4odav966r.cloudfront.net/) 
demonstrating the lambda in action.

## IntelliJ Setup for lombok
We use lombok for auto generation of getters/setter/equals/etc... via
annotations. In order to use this feature with IntelliJ you must do the following:

1. Ensure that you have installed the lombok plugin
    * Go to Preferences -> Plugins
    * Select the "Browse repositories..." button
    * Search for and Install "Lombok Plugin"

2. Go to Preferences -> Build, Execution, Deployment -> Compiler -> Annotation Processors.
    * Check the 'Enable annotation processing' checkbox

## Build
Run the following build command to create an "uber" jar (i.e. one that contains all the 
dependencies as well)

```bash
./gradlew shadowJar
```

## Test
[Spock](https://github.com/spockframework/spock-example) is used for testing. To run tests
perform the following command:

```bash
./gradlew test
```

## Lambda request
The request to the lambda function expects the following payload:
```json
{
  "dateStartRule": "", // OPTIONAL - a string of the form DTSTART=YYYYMMddTHH:mm
  "dateEndRule": "", // OPTIONAL - a string of the form DTSTART=YYYYMMddTHH:mm
  "timezone": "", // REQUIRED - a valid timezone id (e.g. America/Chicago)
  "recurrenceRule": "" // REQUIRED - a valid recurrence rule string (e.g. FREQ=WEEKLY)
}
```

## Lambda response
One or more of the following recurring events:
```json
{
  "id": "", // auto generated UUID
  "startTimeMillis": -1, // epoch millis
  "endTimeMillis": -1 // epoch millis
}
```
