package com.kewley.aws.lambda.recurrence.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Data transfer object for events created
 * from a recurrence rule.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RecurringEventDTO {

    private String id; // a random UUID as string
    private long startTimeMillis;
    private long endTimeMillis;

}
