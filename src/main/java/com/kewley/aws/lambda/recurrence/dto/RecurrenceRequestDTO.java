package com.kewley.aws.lambda.recurrence.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Data transfer object for lambda requests
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RecurrenceRequestDTO {

    private String dateStartRule; // of the form "DTSTART=YYYYMMddTHH:mm"
    private String dateEndRule; // of the form "DTSTART=YYYYMMddTHH:mm"
    private String timezone; // a valid timezone id
    private String recurrenceRule; // a valid recurrence rule (e.g. FREQ=WEEKLY)

}
