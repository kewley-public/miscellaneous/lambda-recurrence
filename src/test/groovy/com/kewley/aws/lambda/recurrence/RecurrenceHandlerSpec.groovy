package com.kewley.aws.lambda.recurrence

import com.amazonaws.services.lambda.runtime.Context
import com.kewley.aws.lambda.recurrence.dto.RecurrenceRequestDTO
import com.kewley.aws.lambda.recurrence.dto.RecurringEventDTO
import com.kewley.aws.lambda.recurrence.service.RecurrenceRuleService
import spock.lang.Specification

class RecurrenceHandlerSpec extends Specification {
    RecurrenceHandler handler = new RecurrenceHandler()

    void setup() {
        handler.recurrenceRuleService = Mock(RecurrenceRuleService)
    }

    void 'dispatches a request to the service and generates unique ids'() {
        given:
        List<RecurringEventDTO> dtos = [RecurringEventDTO.builder().build()]
        RecurrenceRequestDTO request = RecurrenceRequestDTO.builder().build()
        Context context = Mock(Context)

        when:
        List<RecurringEventDTO> events = handler.handleRequest(request, context)

        then:
        1 * handler.recurrenceRuleService.generateRecurringEventsFromRequest(request) >> dtos
        0 * _

        with(events[0]) {
            id != null
        }
    }

}
