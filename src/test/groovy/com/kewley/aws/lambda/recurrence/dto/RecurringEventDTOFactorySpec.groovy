package com.kewley.aws.lambda.recurrence.dto

import spock.lang.Specification

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.Month
import java.time.ZoneOffset

class RecurringEventDTOFactorySpec extends Specification {

    final static ZoneOffset OFFSET = ZoneOffset.of('-05:00')

    void 'createRecurringEventDTOFromMillisAndTime: creates the expected event'() {
        given:
        Long millis = LocalDateTime.of(2018, Month.OCTOBER, 31, 0, 0).toInstant(OFFSET).toEpochMilli()
        LocalTime startTime = LocalTime.of(9, 0)
        LocalTime endTime = LocalTime.of(10, 0)

        when:
        RecurringEventDTO event = RecurringEventDTOFactory.createRecurringEventDTOFromMillisAndTime(
                millis,
                startTime,
                endTime,
                OFFSET
        )

        then:
        with(event) {
            startTimeMillis == LocalDateTime.of(LocalDate.of(2018, Month.OCTOBER, 31), startTime).toInstant(OFFSET).toEpochMilli()
            endTimeMillis == LocalDateTime.of(LocalDate.of(2018, Month.OCTOBER, 31), endTime).toInstant(OFFSET).toEpochMilli()
            id == null
        }
    }

}
